<%@ page language="java" contentType="text/html; charset=windows-1255" import="il.ac.hit.couponsDao.*"
    pageEncoding="windows-1255"%>
<%@page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<title>Delete a coupon</title>
</head>
<body>
	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if(cookies !=null)
		{
			for(Cookie cookie : cookies)
			{
		    	if(cookie.getName().equals("user")) 
		    		userName = cookie.getValue();
			}
		}
		if(userName == null) response.sendRedirect("loginPage.jsp");
	%>
	<h3>Hi <%=userName %>, Login successful.</h3>
	<br>
	<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" >
	</form>

	<% List<Coupon> couponList = (List<Coupon>)request.getSession().getAttribute("listOfCoupons");
	   String message = (String)request.getSession().getAttribute("msg");
	   String msg = message == null ? "" : message;
	   
	   List<String> allCategories = (List<String>)request.getSession().getAttribute("allCategories");
	   List<String> allBusinesses = (List<String>)request.getSession().getAttribute("allBusinesses");
	%>
	<h3 style = "color: red;"><%= msg%></h3>
	<form action = "deleteACoupon.jsp" method ="get">	
	<div align="center">
        <table border="1" cellpadding="5">
            <caption>List of all coupons</caption>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Published Time</th>
                <th>Expired Time</th>
                <th>Price</th>
                <th>Category</th>
                <th>Business</th>
                <th>Is Active</th>
            </tr>
            <% for (Coupon coupon : couponList)
            	{
            		String color = coupon.isActive() == true ? "green" : "red";
            		
					out.print("<tr>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getId());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getDescription());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getPublishedTime());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getExpierdTime());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getPrice());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getCategory());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(coupon.getBusiness());
					out.print("</td>");
					out.print("<td bgcolor=" + color + " align=center>");
					out.print(color == "green" ? "Yes" : "No");
					out.print("</td>");
					out.print("</tr>");
            	}
	%>
        </table>
    </div>
    
    <div align="center"><br><br>
        Choose the ID of the coupon you would like to update or delete:
        	
		    <select id="selectId">
			    <% for (Coupon coupon : couponList)
			    {
			    	out.print("<option>");
					out.print(coupon.getId());
					out.print("</option>");
			    }
			    %>
		    </select>			
    </div>	
	<div align="center">
        <table border="1" cellpadding="5">
            <caption>Selected Coupon</caption>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Published Time</th>
                <th>Expired Time</th>
                <th>Price</th>
                <th>Category</th>
                <th>Business</th>
                <th>Is Active</th>
            </tr>
            <tr>
            	<th><input type = "number" name = "couponId" id = "couponId" style="width: 20px" readonly="readonly"></th>
                <th><input type = "text" name = "description" id = "description" style="width: 100px"></th>
                <th><input type = "datetime-local" name = "publishedTime"  id = "publishedTime" style="width: 180px"></th>
                <th><input type = "datetime-local" name = "expiredTime" id = "expiredTime" style="width: 180px"></th>
                <th><input type = "number" name = "price" id = "price" min = 0 placeholder = 99.99 step = "0.01" style="width: 50px"></th>
                <th><select id="category" name= "category">
              
				    <% for (String category : allCategories)
				    {
				    	out.print("<option>");
						out.print(category);
						out.print("</option>");
				    }
			    
				    %>
			    </select>
			    </th>
			    <th><select id="business" name= "business">
				    <% for (String business : allBusinesses)
				    {
				    	out.print("<option>");
						out.print(business);
						out.print("</option>");
				    }
				    %>
			    </select>
			    </th>
                <th><input type = "checkbox" checked="checked" name = "isActive" id = "isActive" style="width: 30px"></th>
            </tr>
         </table>
         <input type = "submit" name = "submit" value = "Delete">
         <input type = "submit" name = "submit" value = "Update">
         <input type="reset" value= "Clear form">
       </div>    
	</form>

	<script>
    	  $('#selectId').change(function(){  
    		  $('#couponId').val($('#selectId').val());
          });
    	  $('#couponId').val($('#selectId').val());
    </script>
</body>
</html>