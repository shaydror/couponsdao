<%@page import="com.sun.corba.se.impl.protocol.giopmsgheaders.Message"%>
<%@page import="il.ac.hit.couponsDao.CouponsDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Category Or Business</title>
</head>
<body>
	
	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if(cookies !=null)
		{
			for(Cookie cookie : cookies)
			{
		    	if(cookie.getName().equals("user")) 
		    		userName = cookie.getValue();
			}
		}
		if(userName == null) response.sendRedirect("loginPage.jsp");
	%>
	<h3>Hi <%=userName %>, Login successful.</h3>
	<br>
	<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" >
	</form>
	
	<% String msg = (String)request.getSession().getAttribute("msg");
	msg = msg == null ? "" : msg;
	%>
	<form action = "deleteCategory.jsp" method ="get">
		<div>
			<h3 style="color: red;" ><%= msg %></h3>
			<h3>Choose category to delete:</h3>
			<select name ="category">
				<%	
				List<String> allCategories = (List<String>)request.getSession().getAttribute("allCategories");
		 		for (String category : allCategories)
		 		{
		 			out.print("<option>");
	 				out.print(category);
	 				out.print("</option>");
		 		}
				%>
			</select>
			<input type = "submit" name = "deleteCategory" value = "Delete" >
		</div>
	</form>
	<form action = "deleteBusiness.jsp" method ="get">
		<div>
			<h3>Choose business to delete:</h3>
			<select name ="businesses">
				<%	
				List<String> allBusinesses = (List<String>)request.getSession().getAttribute("allBusinesses");	
		 		for (String business : allBusinesses)
		 		{
		 			out.print("<option>");
	 				out.print(business);
	 				out.print("</option>");
		 		}
				%>
			</select>
			<input type = "submit" name = "deleteBusiness" value = "Delete" >
		</div>
	</form>
	<form action = "addCategory.jsp" method ="get">
			<div>
				<h3>Add category</h3>
				<input type = "text" name = "addCategory">
				<input type = "submit" name = "addCategory" value = "Add Category" >
			</div>
		</form>
	<form action = "addBusiness.jsp" method ="get">
		<div>
			<h3>Add business</h3>
			<input type = "text" name = "addBusiness">
			<input type = "submit" name = "addBusiness" value = "Add Business" >
		</div>
	</form>
</body>
</html>