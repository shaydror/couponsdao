<%@ page language="java" contentType="text/html; charset=windows-1255" import="il.ac.hit.couponsDao.*"
    pageEncoding="windows-1255"%>
<%@page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>List of selected coupons</title>
</head>
<body>
	
	<% List<Coupon> couponList = (List<Coupon>)request.getSession().getAttribute("listOfCoupons");
	   List<String> categoryList = (List<String>)request.getSession().getAttribute("allCategories");
	   String selected = (String)request.getSession().getAttribute("selectedCategory");
	%>
	<form action = "couponDetails.jsp" method ="get">
	<div>	
		<select name="selection" id="selection">
			<%	
		 		for (String category : categoryList)
		 		{
		 			out.print("<option>");
		 			out.print(category);
		 			out.print("</option>");
		 		}
			%>
		</select>
		<input type = "submit">
	</div>
	</form>
	<div align="center">
        <table border="1" cellpadding="5">
            <caption>List of all coupons</caption>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Published Time</th>
                <th>Expired Time</th>
                <th>Price</th>
                <th>Category</th>
                <th>Business</th>
            </tr>
            <% for (Coupon coupon : couponList)
            	{ 
            		if (coupon.isActive() && coupon.getCategory().equals(selected))
					{
						out.print("<tr>");
						out.print("<td>");
						out.print(coupon.getId());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getDescription());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getPublishedTime());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getExpierdTime());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getPrice());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getCategory());
						out.print("</td>");
						out.print("<td>");
						out.print(coupon.getBusiness());
						out.print("</td>");
						out.print("</tr>");
					}
            	}
	%>
        </table>
    </div>
 
</body>
</html>