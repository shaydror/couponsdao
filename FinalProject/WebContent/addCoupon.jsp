<%@page import="com.sun.corba.se.impl.protocol.giopmsgheaders.Message"%>
<%@page import="il.ac.hit.couponsDao.CouponsDAO"%>
<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Add Coupon</title>
</head>
<body>
	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if(cookies !=null)
		{
			for(Cookie cookie : cookies)
			{
		    	if(cookie.getName().equals("user")) 
		    		userName = cookie.getValue();
			}
		}
		if(userName == null) response.sendRedirect("loginPage.jsp");
	%>
	<h3>Hi <%=userName %>, Login successful.</h3>
	<br>
	<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" >
	</form>
<%
	String message = (String)request.getSession().getAttribute("msg");
	String msg = message == null ? "" : message;
%>
	<h3 style="color: red;"><%= msg %> </h3>
	<h1>Please add a new coupon</h1>
	<form action = "insertCoupon.jsp" method ="get"> 
	<table>
		<tr>
			<td>Description:</td>
			<td><input type = "text" name = "description"></td>
		</tr>
		<tr>
			<td>Expiration Date:</td>
			<td><input type = "datetime-local" name = "expirationDate"></td>
		</tr>
		<tr>
			<td>Price:</td>
	 		<td><input type = "number" name = "price" min = 0 placeholder = 99.99 step = "0.01"></td>
	 	</tr>
	 	<tr>
	 		<td>Category:</td>
	 		<td>
	 			<select name ="category">
	 			<% CouponsDAO coupondao = CouponsDAO.getInstance();
	 			List<String> allCategories = coupondao.getCategories();
	 			
	 			for (String category : allCategories)
	 			{
	 				out.print("<option>");
	 				out.print(category);
	 				out.print("</option>");
	 			}
	 			%>
	 			</select>
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>Business:</td>
	 		<td>
	 			<select name ="business">
	 			<% List<String> allBusinesses = coupondao.getBusinesses();
	 			for (String business : allBusinesses)
	 			{
	 				out.print("<option>");
	 				out.print(business);
	 				out.print("</option>");
	 			}
	 			%>
	 			</select>
	 		</td>
	 	</tr>
	 	<tr>
			<td>Active Coupon:</td>
			<td><input type = "checkbox" name = "isActive"></td>
		</tr>
		<tr>
			<td colspan="2"><center><input type = "submit" value = "submit"></center></td>
		</tr>
	</table>
	</form>
</body>
</html>