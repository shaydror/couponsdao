package il.ac.hit.couponsDao;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jni.Local;
import org.hibernate.classic.Session;

/**
 * Servlet implementation class servlet
 */
@WebServlet("/Controller/*")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final String userID = "Admin";
    private final String password = "admin";   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();	
		CouponsDAO couponsDAO = CouponsDAO.getInstance();
		
		String path = request.getPathInfo() == null ? "" : request.getPathInfo(); 
		
		if(path.startsWith("/about"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/about.jsp");
			dispatcher.forward(request, response);
		}
		else if(path.startsWith("/addCoupon"))
		{	
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addCoupon.jsp");
			dispatcher.forward(request, response);
		}
		else if(path.startsWith("/couponDetails"))
		{
			List<Coupon> listOfCoupons = couponsDAO.getCoupons();
			request.getSession().setAttribute("listOfCoupons", listOfCoupons);
			List<String> allCategories = couponsDAO.getCategories();
			request.getSession().setAttribute("allCategories", allCategories);
			
			String [] selection = request.getParameterValues("selection");
			String selectedCategory = selection == null ? "" : selection[0];
			request.getSession().setAttribute("selectedCategory", selectedCategory);
			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/couponDetails.jsp");
			dispatcher.forward(request, response);
		}
		else if (path.startsWith("/loginPage"))
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/loginPage.jsp");
			dispatcher.forward(request, response);
		}
		else if (path.startsWith("/insertCoupon"))
		{
			String [] description = request.getParameterValues("description");
			String [] expirationDate = request.getParameterValues("expirationDate");
			String [] price = request.getParameterValues("price");
			String [] category = request.getParameterValues("category");
			String [] business = request.getParameterValues("business");
			String [] isActive = request.getParameterValues("isActive");
			boolean active = isActive == null ? false : true; 
			
			String msg = "Please fill all fileds";
			if (description != null && expirationDate != null && price != null && category != null && business != null)
			{
				if (description[0] != "" && expirationDate[0] != "" && price[0] != "" && category[0] != "" && business[0] != null)
				{
					Coupon couponToAdd = new Coupon();
					couponToAdd.setDescription(description[0]);
					couponToAdd.setPublishedTime(new Date());
					
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
					Date dt = new Date();
					try {
						dt = formatter.parse(expirationDate[0]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					couponToAdd.setExpierdTime(dt);

					double pr = 0;
					try
					{
						pr = Double.parseDouble(price[0]);
					}catch(Exception e)
					{
						pr = 0;
					}
					couponToAdd.setPrice(pr);
					couponToAdd.setCategory(category[0]);
					couponToAdd.setBusiness(business[0]);
					couponToAdd.setActive(active);
					couponsDAO.addCoupon(couponToAdd);
					msg = "Coupon was added succesfully";
				}
			}
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/addCoupon.jsp");
			request.getSession().setAttribute("msg", msg);
			rd.include(request, response);
		}
		else if (path.startsWith("/deleteOrUpdateCoupon"))
		{
			List<Coupon> list = couponsDAO.getCoupons();
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			request.getSession().setAttribute("allCategories", allCategories);
			request.getSession().setAttribute("allBusinesses", allBusinesses);
			request.getSession().setAttribute("listOfCoupons", list);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/deleteOrUpdateCoupon.jsp");
			dispatcher.forward(request, response);
		}
		else if (path.startsWith("/deleteACoupon"))
		{
			String msg = "<font color=red>Please select coupon.</font>";
			String [] submit = request.getParameterValues("submit"); 
			System.out.println(submit[0]);
			if (submit[0].equals("Delete"))
			{
				String [] id = request.getParameterValues("couponId");
				
				if (id != null && id[0] != null)
				{
					boolean ret;
					ret = couponsDAO.deleteCoupon(Integer.parseInt(id[0]));
					if(ret == true)
						msg = "<font color=red>The Coupon was deleted succesfully.</font>";
					else
						msg = "<font color=red>Unable to delete coupon.</font>";
				}
			
			}
			else //update
			{
				String [] id             = request.getParameterValues("couponId");
				String [] description    = request.getParameterValues("description");
				String [] publishedTime  = request.getParameterValues("publishedTime");
				String [] expirationDate = request.getParameterValues("expiredTime");
				String [] price          = request.getParameterValues("price");
				String [] category       = request.getParameterValues("category");
				String [] business       = request.getParameterValues("business");
				String [] isActive       = request.getParameterValues("isActive");
				boolean active           = isActive == null ? false : true; 

				msg = "Please fill all fileds";
				if (id != null && description != null && publishedTime != null && expirationDate != null && price != null && category != null && business != null)
				{
					if (id[0] != null && description[0] != "" && publishedTime[0] != null && expirationDate[0] != "" && price[0] != "" && category[0] != "" && business[0] != null)
					{
						Coupon couponToAdd = new Coupon();//;
						couponToAdd.setId(Integer.parseInt(id[0]));
						couponToAdd.setDescription(description[0]);
						
						
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
						Date dtPublish = new Date();
						try 
						{
							dtPublish = formatter.parse(publishedTime[0]);
						} 
						catch (ParseException e) 
						{
							e.printStackTrace();
						}
						couponToAdd.setPublishedTime(dtPublish);						
						Date dtExpire = new Date();
						try 
						{
							dtExpire = formatter.parse(expirationDate[0]);
						} 
						catch (ParseException e) 
						{
							e.printStackTrace();
						}
						couponToAdd.setExpierdTime(dtExpire);
						
						double pr = 0;
						try
						{
							pr = Double.parseDouble(price[0]);
						}
						catch(Exception e)
						{
							pr = 0;
						}
						couponToAdd.setPrice(pr);
						couponToAdd.setCategory(category[0]);
						couponToAdd.setBusiness(business[0]);
						couponToAdd.setActive(active);
						System.out.println(couponToAdd);
						couponsDAO.updateCoupon(couponToAdd);
						msg = "Coupon was updated succesfully";
					}
				}
			}
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/deleteOrUpdateCoupon.jsp");
				request.getSession().setAttribute("msg", msg);
	            rd.include(request, response);
		}
		else if (path.startsWith("/addDeleteCategoryOrBusiness"))
		{
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			request.getSession().setAttribute("allCategories", allCategories);
			request.getSession().setAttribute("allBusinesses", allBusinesses);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addDeleteCategoryOrBusiness.jsp");
			dispatcher.include(request, response);
		}
		else if (path.startsWith("/addCategory"))
		{
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			String [] category = request.getParameterValues("addCategory");
			String str = "";
			if (category != null && category[0] != "")
			{
				if (!(allCategories.contains(category[0])))
				{
					couponsDAO.addCategory(category[0]);
					str = "Category added sucsessfuly";
				}
				else
				{
					request.getSession().setAttribute("allCategories", allCategories);
					request.getSession().setAttribute("allBusinesses", allBusinesses);
					str = "Category alredy exists";
				}
			}
			else 
			{
				str = "Please insert category!";
			}
			request.getSession().setAttribute("msg", str);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addDeleteCategoryOrBusiness.jsp");
			dispatcher.include(request, response);
		}
		else if (path.startsWith("/addBusiness"))
		{
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			String [] business = request.getParameterValues("addBusiness");
			String str = "";
			if (business != null && business[0] != "")
			{
				if (!(allBusinesses.contains(business[0])))
				{
					couponsDAO.addBusiness(business[0]);
					str = "Businesses added sucsessfuly";
					
				}
				else
				{
					request.getSession().setAttribute("allCategories", allCategories);
					request.getSession().setAttribute("allBusinesses", allBusinesses);
					
					str = "Business alredy exists";
				}
			}
			else
			{
				str = "Please insert business!";
			}
			request.getSession().setAttribute("msg", str);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addDeleteCategoryOrBusiness.jsp");
			dispatcher.include(request, response);
		}
		else if (path.startsWith("/deleteCategory"))
		{
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			String [] category = request.getParameterValues("category");
			String str = "";

			if (category != null && category[0] != null)
			{
				couponsDAO.deleteCategory(category[0]);
				str = "Category deleted sucssesfuly";
			}
			request.getSession().setAttribute("allCategories", allCategories);
			request.getSession().setAttribute("allBusinesses", allBusinesses);
			request.getSession().setAttribute("msg", str);

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addDeleteCategoryOrBusiness.jsp");
			dispatcher.include(request, response);
		}
		else if (path.startsWith("/deleteBusiness"))
		{
			List<String> allCategories = couponsDAO.getCategories();
			List<String> allBusinesses = couponsDAO.getBusinesses();
			String [] business = request.getParameterValues("businesses");
			String str = "";
			
			if (business != null && business[0] != null)
			{
				couponsDAO.deleteBusiness(business[0]);
				str = "Business deleted sucssesfuly";
			}
			request.getSession().setAttribute("allCategories", allCategories);
			request.getSession().setAttribute("allBusinesses", allBusinesses);
			request.getSession().setAttribute("msg", str);

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/addDeleteCategoryOrBusiness.jsp");
			dispatcher.include(request, response);
		}
		//home page
		else
		{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/homePage.html");
			dispatcher.forward(request, response);
			CouponsDAO c = CouponsDAO.getInstance();
			System.out.println(c.getCouponIdByDescription("car"));
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
 
		String path = request.getPathInfo() == null ? "" : request.getPathInfo(); 
		
		if (path.startsWith("/LoginServlet"))
		{
	        // get request parameters for userID and password
	        String user = request.getParameter("user");
	        String pwd = request.getParameter("pwd");
	         
	        if(userID.equals(user) && password.equals(pwd)){
	            Cookie loginCookie = new Cookie("user",user);
	            //setting cookie to expiry in 30 mins
	            loginCookie.setMaxAge(30*60);
	            response.addCookie(loginCookie);
	            response.sendRedirect("LoginSuccess.jsp");
	        }
	        else
	        {
	            RequestDispatcher rd = getServletContext().getRequestDispatcher("/loginPage.jsp");
	            PrintWriter out= response.getWriter();
	            out.println("<font color=red>Either user name or password is wrong.</font>");
	            rd.include(request, response);
	        }
		}
		else if ((path.startsWith("/LogoutServlet")))
		{
			 response.setContentType("text/html");
		        Cookie loginCookie = null;
		        Cookie[] cookies = request.getCookies();
		        if(cookies != null){
		        for(Cookie cookie : cookies){
		            if(cookie.getName().equals("user")){
		                loginCookie = cookie;
		                break;
		            }
		        }
		        }
		        if(loginCookie != null){
		            loginCookie.setMaxAge(0);
		            response.addCookie(loginCookie);
		        }
		        RequestDispatcher rd = getServletContext().getRequestDispatcher("/loginPage.jsp");
		        rd.include(request, response);
		}
    }

}
