package il.ac.hit.couponsDao;

import java.util.*;

public interface ICouponsDAO {
	   public abstract Coupon getCoupon(int id);
	   public abstract boolean updateCoupon(Coupon ob);
	   public abstract List<Coupon> getCoupons();
	   public abstract boolean deleteCoupon(int id);
	   public abstract boolean addCoupon(Coupon ob);
}
