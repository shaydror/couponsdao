package il.ac.hit.couponsDao;

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
//import org.apache.log4j.BasicConfigurator;
//import org.apache.log4j.Logger;

public class CouponsDAO implements ICouponsDAO
{
	private static CouponsDAO coupons;
	private static SessionFactory factory;
	private String className = "Coupon";
	//static Logger logger = Logger.getLogger(CouponsDAO.class);
	private CouponsDAO()
	{
		super();
		//BasicConfigurator.configure();
		factory = new AnnotationConfiguration().configure().buildSessionFactory();
		//logger.debug("CouponsDAO created!");
	}
	public static CouponsDAO getInstance()
	{
		if (coupons == null)
			coupons = new CouponsDAO();
		return coupons;	
	}
	@Override
	public Coupon getCoupon(int id) 
	{
		Session session  = null;
		Coupon ob = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
		    ob = (Coupon)session.get(Coupon.class,id);	
		    return ob;
		}
		catch(HibernateException e)
		{
			System.out.println("exception in getting coupn: " + e.toString());
			return null;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}
	@Override
	public boolean updateCoupon(Coupon ob) 
	{
		Session session = null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			session.update(ob);
			tx.commit();
			return true;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in updating coupon: " + e.toString());
			if (tx!=null) 
				tx.rollback();
			return false;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}
	@Override
	public List<Coupon> getCoupons() 
	{
		Session session = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
			String str =  "from " + className + " where description not like '@%'";
			List<Coupon> list = session.createQuery(str).list();
			
			for (Coupon coupon : list)
			{
				if (isExpired(coupon) && coupon.isActive())
				{
					coupon.setActive(false);
					updateCoupon(coupon);
				}
			}
			return list;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in getting all coupons: " + e.toString());
			return null;
		}
		finally
		{
			if (session != null)
				session.close();
		}	
	}
	@Override
	public boolean deleteCoupon(int id)
	{
		Session session = null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			Coupon couponToDelete = this.getCoupon(id);
			session.delete(couponToDelete);
			tx.commit();
			return true;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in deleting coupon: " + e.toString());
			if (tx!=null) 
				tx.rollback();
			return false;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}
	public boolean deleteCategory(String category)
	{
		Session session = null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			String str =  "DELETE from " + className + " where category = :category";
			Query query = session.createQuery(str);
			query.setParameter("category", category);
			query.executeUpdate();
			tx.commit();
			return true;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in deleting category: " + e.toString());
			if (tx!=null) 
				tx.rollback();
			return false;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}
	public boolean deleteBusiness(String business)
	{
		Session session = null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			String str =  "DELETE from " + className + " where business = :business";
			Query query = session.createQuery(str);
			query.setParameter("business", business);
			query.executeUpdate();
			tx.commit();
			return true;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in deleting business: " + e.toString());
			if (tx!=null) 
				tx.rollback();
			return false;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}	
	@Override
	public boolean addCoupon(Coupon ob)
	{
		Session session = null;
		Transaction tx = null;
		try
		{
			session = factory.openSession();
			tx = session.beginTransaction();
			session.merge(ob);
			tx.commit();	
			return true;
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in adding coupon: " + e.toString());
			if (tx!=null) 
				tx.rollback();
			return false;
		}
		finally
		{
			if (session != null)
				session.close();
		}
	}
	public boolean isExpired(Coupon coupon)
	{
		Date currentDate = new Date();
		Date expierdDate = coupon.getExpierdTime();
		return (currentDate.after(expierdDate));
	}
	@SuppressWarnings("unchecked")
	public List<String> getCategories()
	{
		Session session = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
			String str = "select distinct category from " + className + " where Description = '@Category'";
			return session.createQuery(str).list();
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in getting all categories: " + e.toString());
			return null;
		}
		finally
		{
			if (session != null)
				session.close();
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getBusinesses()
	{
		Session session = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
			String str = "select distinct business from " + className + " where Description = '@Business'";
			return  session.createQuery(str).list();
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in getting all businesses: " + e.toString());
			return null;
		}
		finally
		{
			if (session != null)
				session.close();
		}		
	}
	
	public boolean addCategory(String category)
	{
		Coupon coupon = new Coupon("@Category", new Date(), new Date(), 0.0, category, "", false);
		return this.addCoupon(coupon);
	}
	
	public boolean addBusiness(String business)
	{
		Coupon coupon = new Coupon("@Business", new Date(), new Date(), 0.0, "", business, false);
		return this.addCoupon(coupon);
	}
	
	public int getCouponIdByDescription(String description)
	{
		Session session = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
			String str = "select id from " + className + " where description = '"+ description +"'";
			List<Integer> list = session.createQuery(str).list();
			return  list.get(0);
		}
		catch(HibernateException e)
		{
			//logger.debug("exception in serching id by description: " + e.toString());
			return 0;
		}
		finally
		{
			if (session != null)
				session.close();
		}
		
		
	}
}
