package il.ac.hit.couponsDao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Coupon{

	private int id;
	private String description;
	private Date publishedTime;
	private Date expierdTime;
	private double price;
	private boolean active;
	private String category;
	private String business;
	
	public Coupon()
	{
		super();		
	}
	public Coupon(String description, Date publishedTime,
			Date expierdTime, double price, String category, String business, boolean active) {
		super();
		setDescription(description);
		setPublishedTime(publishedTime);
		setExpierdTime(expierdTime);
		setPrice(price);
		setCategory(category);
		setBusiness(business);
		setActive(active);
	}

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		if (category != null)
			this.category = category;
		else
			this.category = "Default";
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		if (business != null)
			this.business = business;
		else
			this.business = "Default";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
			this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if (description != null)
			this.description = description;
		else
			this.description = "No Description";
	}

	public Date getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(Date publishedTime)
	{
		this.publishedTime = publishedTime;
	}

	public Date getExpierdTime() {
		return expierdTime;
	}
	public void setExpierdTime(Date expierdTime) 
	{		
		this.expierdTime = expierdTime;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		if (price > 0)
			this.price = price;
	}

	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@Override
	public String toString() {
		return "Coupon [id=" + id + ", description=" + description
				+ ", publishedTime=" + publishedTime + ", expierdTime="
				+ expierdTime + ", price=" + price + ", active=" + active
				+ ", category=" + category + ", business=" + business + "]";
	}
}
